<?php 

namespace classes;

use Generator;
use Sentiment\Analyzer;

class Dataset
{
    private Analyzer $analyzer;
    private string $filename;

    public function __construct(Analyzer $analyzer, string $filename)
    {
        $this->analyzer = $analyzer;
        $this->filename = $filename;
    }

    public function getPositive(): Generator
    {
        foreach ($this->load() as $line) {
            [$name, $description] = $line;
            $sentiment = $this->analyzer->getSentiment($description);
            if ($sentiment['pos'] > 0) {
                yield [$name, $description];
            }
        }
    }

    public function getNegative(): Generator
    {
        foreach ($this->load() as $line) {
            [$name, $description] = $line;
            $sentiment = $this->analyzer->getSentiment($description);
            if ($sentiment['neg'] > 0) {
                yield [$name, $description];
            }
        }
    }

    public function getByType(int $type): Generator
    {
        if ($type === 0) {
            return $this->getPositive();
        }

        return $this->getNegative();
    }

    private function load(): Generator
    {
        $file = fopen($this->filename, 'r');
        fgetcsv($file);
        try {
            while ($line = fgetcsv($file)) {
                yield $line;
            }
        } finally {
            fclose($file);
        }
    }
}
<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require './vendor/autoload.php';

use classes\Dataset;
use Sentiment\Analyzer;

$filename = './dataset.csv';
$analyzer = new Analyzer();

$dataset = new Dataset($analyzer, $filename);
?>
<div class="p-3">
    <a href="?type=0" class="btn btn-primary m-1">Positive</a>
    <a href="?type=1" class="btn btn-primary m-1">Negative</a>
</div>
<hr>

<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    </head>
    <body>
        <h1><?=(!empty($_GET['type']) ? 'Negative' : 'Positive')?></h1>
        <hr>
        <table class="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($dataset->getByType(!empty($_GET['type']) ? 1 : 0) as $data) {
                [$name, $desc] = $data;
                ?>
                <tr>
                    <td><?=$name?></td>
                    <td><?=$desc?></td>
                </tr>
                <?php
            } ?>
            </tbody>
        </table>
    </body>
</html>